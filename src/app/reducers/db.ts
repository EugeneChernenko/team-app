import * as Db from '../actions/db';
import { Player } from '../shared/player';

export function reducer(state = [], action: Db.Actions): Player[] {
  switch (action.type) {

    case Db.RECIEVE_PLAYERS:
      return action.payload;

    case Db.ADD_PLAYER_SUCCESS:
      return [...state, action.payload];

    case Db.EDIT_PLAYER_SUCCESS:
      const index = state.findIndex((player) => player._id === action.payload._id);
      return [
        ...state.slice(0, index),
        action.payload,
        ...state.slice(index + 1)
      ];

    case Db.DELETE_PLAYER_SUCCESS:
      return state.filter(player => {
        return player._id !== action.payload;
      });

    default:
      return state;

  }
}
