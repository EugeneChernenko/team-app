import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';

import * as fromDb from './db';
import { Player } from '../shared/player';

import { environment } from '../../environments/environment';

export interface State {
  players: Player[];
}

export const reducers: ActionReducerMap<State> = {
  players: fromDb.reducer,
};

export const getPlayersState = (state: State) => state.players;
export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
