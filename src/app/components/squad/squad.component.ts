import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import * as fromRoot from '../../reducers/index';
import { Player } from '../../shared/player';
import { PlayerDetailComponent } from '../player-detail/player-detail.component';
import { Popup } from '../popup/popup';
import * as Db from '../../actions/db';
import { PopupComponent } from '../popup/popup.component';
import { PopupModes } from '../popup/popup-modes';
import { mockPlayer } from './mock-player';

@Component({
  selector: 'app-squad',
  templateUrl: './squad.component.html',
  styleUrls: ['./squad.component.scss']
})
export class SquadComponent implements OnInit, OnDestroy {
  private players$: Observable<Player[]> = this.store.select(fromRoot.getPlayersState);
  private playersSubscription: Subscription;
  private squad: Player[] = [];
  private editPlayer: Player;
  private selectedPlayer: Player;
  private popup: Popup;
  private popupTitle: string;
  private PopupModes = PopupModes;
  private mockPlayer: Player = mockPlayer;

  constructor(public store: Store<fromRoot.State>) {}

  ngOnInit() {
    this.store.dispatch(new Db.RequestPlayers());
    this.playersSubscription = this.players$.subscribe(squad => this.squad = squad);
  }

  private openPopup(player: Player, mode) {
    this.editPlayer = Object.assign({}, player);
    this.popupTitle = mode.title;
    this.popup = {
      open: true,
      mode
    };
  }
  private closePopup() {
    this.popup.open = false;
  }
  private selectPlayer(event, player: Player) {
    event.preventDefault();
    this.selectedPlayer = player;
  }

  ngOnDestroy() {
    this.playersSubscription.unsubscribe();
  }
}

