const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

const dbName = 'team';
var PLAYERS_COLLECTION = 'players';

let db;

// Connect
const connection = (closure) => {
    return MongoClient.connect('mongodb://localhost:27017', (err, client) => {
        if (err) return console.log(err);

        db = client.db(dbName);
        closure(db);
    });
};

// Error handling
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
};

// GET: finds all players
router.get('/players', function(req, res) {
  connection((db) => {
    db.collection(PLAYERS_COLLECTION).find({}).toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, 'Failed to get players.');
      } else {
        res.status(200).json(docs);
      }
    })
  })
});

// POST: creates a new player
router.post("/players", function(req, res) {
  var newPlayer = req.body;
  newPlayer.createDate = new Date();
  connection((db) => {
    db.collection(PLAYERS_COLLECTION).insertOne(newPlayer, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to create new player.");
      } else {
        res.status(201).json(doc.ops[0]);
      }
    });
  })
});

//PUT: update player by id
router.put("/players/:id", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc._id;
  connection((db) => {
    db.collection(PLAYERS_COLLECTION).updateOne({_id: new ObjectID(req.params.id)}, { $set: updateDoc }, { upsert: true }, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to update player");
      } else {
        updateDoc._id = req.params.id;
        res.status(200).json(updateDoc);
      }
    })
  })
});

//DELETE: delete player by id
router.delete("/players/:id", function(req, res) {
  connection((db) => {
    db.collection(PLAYERS_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
      if (err) {
        handleError(res, err.message, "Failed to delete player");
      } else {
        res.status(200).json(req.params.id);
      }
    })
  })
});

module.exports = router;
