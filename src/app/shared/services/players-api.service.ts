import { Injectable } from '@angular/core';
import { DbService as Db } from './db.service';
import { Player } from '../player';

import { Observable, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayersApiService {

  constructor(private DbServ: Db) { }

  public getAll(): Observable<Player[]> {
    return from(new Promise((resolve, reject) => {
      return resolve(this.DbServ.getPlayers());
    }));
  }

  public addNewPlayer(newPlayer: Player): Observable<Player> {
    return from(new Promise((resolve, reject) => {
      return resolve(this.DbServ.addNewPlayer(newPlayer));
    }));
  }

  public updatePlayer(player: Player): Observable<Player> {
    return from(new Promise((resolve, reject) => {
      return resolve(this.DbServ.updatePlayer(player));
    }));
  }

  public deletePlayer(id: string): Observable<Player> {
    return from(new Promise((resolve, reject) => {
      return resolve(this.DbServ.deletePlayer(id));
    }));
  }
}
