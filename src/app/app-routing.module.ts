import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { AchievementsComponent } from './components/achievements/achievements.component';
import { ContactComponent } from './components/contact/contact.component';
import { HistoryComponent } from './components/history/history.component';
import { SquadComponent} from './components/squad/squad.component';
import { StatsComponent } from './components/stats/stats.component';
import { VideosComponent } from './components/videos/videos.component';

const routes: Routes = [
  { path: '', redirectTo: '/about', pathMatch: 'full' },
	{ path: 'about', component: AboutComponent },
	{ path: 'achievements', component: AchievementsComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'squad', component: SquadComponent },
  { path: 'stats', component: StatsComponent },
  { path: 'videos', component: VideosComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
