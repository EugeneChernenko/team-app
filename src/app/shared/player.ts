export class Player {
// tslint:disable-next-line: variable-name
  _id?: number;
  firstName: string;
  lastName: string;
  age: number;
  position: string;
  shirtNumber: number;
  gamesPlayed: number;
  goalsScored: number;
  yellowCards: number;
  redCards: number;
  captain: boolean;
  image?: string;
}
