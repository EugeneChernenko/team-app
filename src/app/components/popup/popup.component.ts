import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import * as Db from '../../actions/db';
import * as fromRoot from '../../reducers/index';
import { Player } from '../../shared/player';
import { Popup } from './popup';
import { PopupModes } from '../popup/popup-modes';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  private PopupModes = PopupModes;

  @Input()
  editPlayer: Player;

  @Input()
  popup: Popup;

  @Input()
  popupTitle: string;

  constructor(public store: Store<fromRoot.State>) {}

  ngOnInit() {
  }
  private submit() {
    if (this.popup.mode === this.PopupModes.ADD) {
      this.addNewPlayer(this.editPlayer);
    }
    if (this.popup.mode === this.PopupModes.EDIT) {
      this.updatePlayer(this.editPlayer);
    }
    if (this.popup.mode === this.PopupModes.DELETE_ONE) {
      this.deletePlayer(this.editPlayer);
    }
    this.popup.open = false;
  }
  private addNewPlayer(newPlayer: Player) {
    if (!this.isValid(newPlayer)) { return; }
    this.store.dispatch(new Db.AddPlayer(newPlayer));
  }
  private updatePlayer(player: Player) {
    if (!this.isValid(player)) { return; }
    this.store.dispatch(new Db.EditPlayer(player));
  }
  private deletePlayer(player: Player) {
    this.store.dispatch(new Db.DeletePlayer(player._id));
  }
  private cancel() {
    this.popup.open = false;
  }
  private isValid(player: Player) {
    return true;
  }
}
