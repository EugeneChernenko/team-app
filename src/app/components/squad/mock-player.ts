import { Player } from '../../shared/player';

export const mockPlayer: Player = {
  firstName: '',
  lastName: '',
  age: 0,
  position: 'sting',
  shirtNumber: 0,
  gamesPlayed: 0,
  goalsScored: 0,
  yellowCards: 0,
  redCards: 0,
  captain: false,
  image: ''
};
