import { Injectable } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { switchMap, mergeMap, map, catchError } from 'rxjs/operators';

import { PlayersApiService } from '../shared/services/players-api.service';
import * as Db from '../actions/db';



@Injectable()
export class DbEffects {

  @Effect()
  requestPlayers$: Observable<Db.DbAction> = this.actions$
    .pipe(
      ofType(Db.REQUEST_PLAYERS),
      switchMap(actions => this.PlayersApiServ.getAll()
        .pipe(
          switchMap(players => of(new Db.RecievePlayers(players))),
          catchError(err => {
            console.log('err', err);
            return of(new Db.RequestFailed(err));
          })
        )
      )
    );



  @Effect()
  addPlayer$: Observable<Db.DbAction> = this.actions$
    .pipe(
      ofType(Db.ADD_PLAYER),
      switchMap((action: Db.DbAction) => this.PlayersApiServ.addNewPlayer(action.payload)
        .pipe(
          switchMap(player => of(new Db.AddPlayerSuccess(player))),
          catchError(err => {
            console.log('err', err);
            return of(new Db.RequestFailed(err));
          })
        )
      )
    );

  @Effect()
  editPlayer$: Observable<Db.DbAction> = this.actions$
  .pipe(
    ofType(Db.EDIT_PLAYER),
    switchMap((action: Db.DbAction) => this.PlayersApiServ.updatePlayer(action.payload)
      .pipe(
        switchMap(player => of(new Db.EditPlayerSuccess(player))),
        catchError(err => {
          console.log('err', err);
          return of(new Db.RequestFailed(err));
        })
      )
    )
  );

  @Effect()
  deletePlayer$: Observable<Db.DbAction> = this.actions$
  .pipe(
    ofType(Db.DELETE_PLAYER),
    switchMap((action: Db.DbAction) => this.PlayersApiServ.deletePlayer(action.payload)
      .pipe(
        switchMap(id => of(new Db.DeletePlayerSuccess(id))),
        catchError(err => {
          console.log('err', err);
          return of(new Db.RequestFailed(err));
        })
      )
    )
  );

  constructor(
    private actions$: Actions,
    private PlayersApiServ: PlayersApiService
  ) { }
}
