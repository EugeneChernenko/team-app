import { Injectable } from '@angular/core';
import { Player } from '../player';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  private playersUrl = '/api/players';

  constructor(private http: HttpClient) { }

  async getPlayers(): Promise<Player[]> {
    const response = await this.http.get<Player[]>(this.playersUrl)
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      )
      .toPromise();
    return response as Player[];
  }

  async addNewPlayer(newPlayer: Player): Promise<Player>  {
    const response = await this.http.post<Player>(this.playersUrl, newPlayer)
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      )
      .toPromise();
    return response as Player;
  }

  async updatePlayer(player: Player): Promise<Player>  {
    const url = `${this.playersUrl}/${player._id}`;
    const response = await this.http.put<Player>(url, player)
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      )
      .toPromise();
    return response as Player;
  }

  async deletePlayer(id: string): Promise<Player>  {
    const url = `${this.playersUrl}/${id}`;
    const response = await this.http.delete<Player>(url)
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      )
      .toPromise();
    return response as Player;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
        // console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
