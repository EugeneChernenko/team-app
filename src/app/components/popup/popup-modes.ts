export const PopupModes = {
  ADD: {
    mode: 'add',
    title: 'Add new player'
  },
  EDIT: {
    mode: 'edit',
    title: 'Edit player'
  },
  DELETE_ONE: {
    mode: 'Delete one',
    title: 'Please confirm player deleting'
  },
  DELETE_ALL: {
    mode: 'Delete all',
    title: 'Please confirm all players deleting'
  }
}
