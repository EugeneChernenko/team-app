import { Action } from '@ngrx/store';
import { Player } from '../shared/player';

export const REQUEST_PLAYERS = '[Db] Request players';
export const RECIEVE_PLAYERS = '[Db] Recieve players';
export const ADD_PLAYER = '[Db] Add player';
export const ADD_PLAYER_SUCCESS = '[Db] Add player success';
export const EDIT_PLAYER = '[Db] Edit player';
export const EDIT_PLAYER_SUCCESS = '[Db] Edit player success';
export const DELETE_PLAYER = '[Db] Delete player';
export const DELETE_PLAYER_SUCCESS = '[Db] Delete player success';
export const REQUEST_FAILED = '[Db] Request failed';

export interface DbAction extends Action {
  payload?: any;
}

export class RequestPlayers implements DbAction {
  readonly type = REQUEST_PLAYERS;
}

export class RecievePlayers implements DbAction {
  readonly type = RECIEVE_PLAYERS;

  constructor(public payload: Player[]) { }
}

export class AddPlayer implements DbAction {
  readonly type = ADD_PLAYER;

  constructor(public payload: any) { }
}

export class AddPlayerSuccess implements DbAction {
  readonly type = ADD_PLAYER_SUCCESS;

  constructor(public payload: any) { }
}

export class EditPlayer implements DbAction {
  readonly type = EDIT_PLAYER;

  constructor(public payload: any) { }
}

export class EditPlayerSuccess implements DbAction {
  readonly type = EDIT_PLAYER_SUCCESS;

  constructor(public payload) { }
}

export class DeletePlayer implements DbAction {
  readonly type = DELETE_PLAYER;

  constructor(public payload: any) { }
}

export class DeletePlayerSuccess implements DbAction {
  readonly type = DELETE_PLAYER_SUCCESS;

  constructor(public payload: any) { }
}

export class RequestFailed implements DbAction {
  readonly type = REQUEST_FAILED;

  constructor(public error: any) { }
}

export type Actions =
  | RequestPlayers
  | RecievePlayers
  | AddPlayer
  | AddPlayerSuccess
  | EditPlayer
  | EditPlayerSuccess
  | DeletePlayer
  | DeletePlayerSuccess
  | RequestFailed;
